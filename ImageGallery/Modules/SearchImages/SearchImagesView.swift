//
//  SearchImagesView.swift
//  ImageGallery
//
//  Created by MA Maniar on 21/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import UIKit

class SearchImagesView: BaseView {
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var navBar: UIView!
    
    //MARK: Overriders
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = SearchImageViewModel()
        
        self.setCollectionView()
        self.setTextField()
        self.setNavigationBar()
        
        self.bindWithReloadData()
        self.bindWithEmptyView()
        
        self.addGesture()
    }
    
    private func setTextField() {
        self.searchField.delegate = self
        
        
        
        searchField.layer.shadowColor = UIColor.lightGray.cgColor
        searchField.layer.shadowOpacity = 1
        searchField.layer.shadowOffset = .zero
        searchField.layer.shadowRadius = 3
        
    }
    
    private func setNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.navBar.fadeView(style: .bottom, percentage: 0.7)
        }
        
    }
    
    private func addGesture() {
        let gesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        self.collectionView.addGestureRecognizer(gesture)
    }
    @objc func pinchGesture (_ gesture: UIPinchGestureRecognizer) {
        let gestureScale =  Int(gesture.scale * 10.0)
        if gestureScale > 1 && gestureScale < 9{
            DispatchQueue.main.async {
                self.setLayout(column: gestureScale)
            }
        }
    }
    
    /// Private function to setCollectionView
    private func setCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
        
        self.setLayout(column: 2)

    }
        
    private func setLayout(column: Int) {
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        // Change individual layout attributes for the spacing between cells
        let distance = CGFloat(10 - column)
        layout.minimumColumnSpacing = distance
        layout.minimumInteritemSpacing = distance
        layout.columnCount = column
        
        layout.sectionInset = UIEdgeInsets(top: distance, left: distance, bottom: distance, right: distance)
        
        // Collection view attributes
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        collectionView.collectionViewLayout = layout
    }
    
    //MARK:- Bindings
    func bindWithReloadData(){
        (viewModel as! SearchImageViewModel).refreshData = {
            DispatchQueue.main.async {
                //                self.scrollView.contentSize = self.collectionView.contentSize
                self.collectionView.reloadData()
            }
            
        }
    }
    func bindWithEmptyView(){
        (viewModel as! SearchImageViewModel).showEmptyView = { shouldShow, text in
            DispatchQueue.main.async {
                self.emptyView.isHidden = !shouldShow
                self.emptyLabel.text = text
            }
            
        }
    }
}

/// Setting UICollectionViewDataSource
extension SearchImagesView : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel as! SearchImageViewModel).images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.obj = (viewModel as! SearchImageViewModel).images[indexPath.row]
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (viewModel as! SearchImageViewModel).getMoreImages(index: indexPath.row)
    }
}
/// Setting UICollectionViewDelegate
extension SearchImagesView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
extension SearchImagesView: CHTCollectionViewDelegateWaterfallLayout {
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
        let obj = (viewModel as! SearchImageViewModel).images[indexPath.row]
        let imageSize = CGSize(width: obj.width, height: obj.height)
        return imageSize
    }
}

extension SearchImagesView : FavoriteDelegate {
    
    //Callback when favorite is tapped.
    func didFavoriteImage(index: IndexPath) {
        (viewModel as! SearchImageViewModel).setImageFavorite(index: index.row)
    }
    
}

extension SearchImagesView : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        //call after user stop writing in search
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(SearchImagesView.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(SearchImagesView.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }
    
    @objc func getHintsFromTextField(textField: UITextField) {
        (viewModel as! SearchImageViewModel).searchText = textField.text
        (viewModel as! SearchImageViewModel).getImages()
    }
}
