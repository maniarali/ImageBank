//
//  SearchImageViewModel.swift
//  ImageGallery
//
//  Created by MA Maniar on 21/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

class SearchImageViewModel : BaseViewModel {
    
    override init() {
        super.init()
    }
    
    private var imageRepository: ImageRepository = ImageRepository()
    
    var images:[HomeImages] = []
    
    var refreshData: (() -> Void)?
    var showEmptyView:((Bool, String) -> Void)?
    
    var page : Pagination?
    var pageNumber = 1
    
    var searchText: String? {
        didSet {
            self.page = nil
        }
    }
    
    //Operation for fetching image
    func getImages(){
        
        guard let text = searchText, text != "" else { return }
        
        imageRepository.getSearchImages(query: text, pageNumber: pageNumber, completion:{ (data, pagination) in
            self.isLoading = false
            self.showEmptyView?(data?.results.count == 0, "No image found for \(self.searchText ?? "")")
            if self.page == nil {
                self.images.removeAll()
            }
            guard let imageResponse = data?.results else {
                return
            }
            guard let pageResponse = pagination else {
                return
            }
            for obj in imageResponse {
                self.images.append(
                    HomeImages(id: obj.id,
                               height: obj.height,
                               width: obj.width,
                               color: obj.color,
                               thumbImage: obj.urls.thumb,
                               mediumImage: obj.urls.regular,
                               largeImage: obj.urls.full,
                               rawImage: obj.urls.raw,
                               smallImage: obj.urls.small))
            }
            
            self.pageNumber += 1
            
            self.page = pageResponse
            self.refreshData?()
            
        }) { (serverErrorResponse, message) in
            self.isLoading = false
            
            if let message = serverErrorResponse?.message {
                self.errorMessage = message
                return
                
            }
            
            //parse serverErrorResponse.errors for keybased errors
            if let message = message {
                self.errorMessage = message
                return
            }
            
        }
    }
    func getMoreImages (index:Int) {
        if index == (self.images.count - 1) {
            if let pageObj = page, self.pageNumber <= pageObj.total_pages   {
                self.getImages()
            }
        }
    }
    func setImageFavorite(index:Int) {
        
        let obj = self.images[index]
        let image = ImageModel(id: obj.id, urls: ImageURL(raw: obj.rawImage, full: obj.largeImage, regular: obj.mediumImage, small: obj.smallImage, thumb: obj.thumbImage), height: obj.height, width: obj.width, color: obj.color)
        
        imageRepository.saveFavoriteImage(image: image, completion: { (success) in
            print(success)
        }) { error in
            print(error)
        }
    }
}
