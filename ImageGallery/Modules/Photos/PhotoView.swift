//
//  PhotoView.swift
//  ImageGallery
//
//  Created by MA Maniar on 20/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import UIKit

class PhotoView: BaseView {
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var emptyView: UIView!
    
    //MARK: Overriders
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = PhotoViewModel()
        
        self.setCollectionView()
        self.setNavigationBar()
        
        self.bindWithReloadData()
        self.bindWithEmptyView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        (viewModel as! PhotoViewModel).getImages()
    }

    private func setNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.navBar.fadeView(style: .bottom, percentage: 0.7)
        }
        
    }
    
    /// Private function to setCollectionView
    private func setCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        layout.columnCount = 2
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        // Collection view attributes
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        collectionView.collectionViewLayout = layout

    }
    
    //MARK:- Bindings
    func bindWithReloadData(){
        (viewModel as! PhotoViewModel).refreshData = {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }
    }
    func bindWithEmptyView(){
        (viewModel as! PhotoViewModel).showEmptyView = { shouldShow in
            DispatchQueue.main.async {
                self.emptyView.isHidden = !shouldShow
            }
            
        }
    }
    
}

extension PhotoView : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel as! PhotoViewModel).images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.obj = (viewModel as! PhotoViewModel).images[indexPath.row]
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    
}
extension PhotoView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}
extension PhotoView: CHTCollectionViewDelegateWaterfallLayout {
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
        let obj = (viewModel as! PhotoViewModel).images[indexPath.row]
        let imageSize = CGSize(width: obj.width, height: obj.height)
        return imageSize
    }
}

extension PhotoView : FavoriteDelegate {
    
    func didFavoriteImage(index: IndexPath) {
        (viewModel as! PhotoViewModel).unSetImageFavorite(index: index.row)
    }
    
}
