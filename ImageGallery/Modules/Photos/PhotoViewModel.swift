//
//  PhotoViewModel.swift
//  ImageGallery
//
//  Created by MA Maniar on 20/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

class PhotoViewModel : BaseViewModel {
    
    override init() {
        super.init()
        
    }
    
    private var imageRepository: ImageRepository = ImageRepository()
    
    var images:[HomeImages] = []
    var refreshData:(() -> Void)?
    var showEmptyView:((Bool) -> Void)?
    
    //Operation for fetching those are save
    func getImages(){
        
        self.isLoading = true
        imageRepository.getFavoriteImage(completion: { (data) in
            self.isLoading = false
            self.images = []
            self.showEmptyView?(data.count == 0)
            for obj in data {
                
                self.images.append(
                    HomeImages(id: obj.id,
                               height: obj.height,
                               width: obj.width,
                               color: obj.color,
                               thumbImage: obj.urls.thumb,
                               mediumImage: obj.urls.regular,
                               largeImage: obj.urls.full,
                               rawImage: obj.urls.raw,
                               smallImage: obj.urls.small))
            }
            self.refreshData?()
            
        }) { (message) in
            //parse serverErrorResponse.errors for keybased errors
            self.isLoading = false
            if let message = message {
                self.errorMessage = message
                return
            }
        }
    }
    
    func unSetImageFavorite(index:Int) {
        
        let obj = self.images[index]
        
        imageRepository.unSaveFavoriteImage(id: obj.id, completion: { (success) in
            self.getImages()
        }) { error in
            print(error)
        }
    }
}

