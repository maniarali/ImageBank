//
//  BaseViewModel.swift
//  ImageGallery
//
//  Created by MA Maniar on 18/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation


class BaseViewModel: NSObject {

    var setFailureMessage : ((String) -> Void)?
    var setLoading : ((Bool) -> Void)?
    var setToastView : ((String) -> Void)?

    var isLoading : Bool = false {
        didSet {
            setLoading?(isLoading)
        }
    }
    var errorMessage : String = "" {
        didSet {
            setFailureMessage?(errorMessage)
        }
    }
    var setToastMessage : String = "" {
        didSet {
            setToastView?(setToastMessage)
        }
    }
//    var networkAdapter : NetworkChangeNotifiable = NetworkChangeClass()
}
