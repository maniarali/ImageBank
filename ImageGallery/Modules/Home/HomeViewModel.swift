//
//  HomeViewModel.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

class HomeViewModel : BaseViewModel {
    
    
    override init() {
        super.init()
        
        getImages()
    }
    
    private var imageRepository: ImageRepository = ImageRepository()
    
    var images:[HomeImages] = []
    var refreshData: (() -> Void)?
    var page : Pagination?
    
    var reloadHeader:(() -> Void)?
    var pageNumber = 1
    
    
    
    func getImages(){
        
        imageRepository.getImages(pageNumber: pageNumber, completion:{ (data) in
            self.isLoading = false
            for obj in data {
                self.images.append(HomeImages(id: obj.id,
                                              height: obj.height,
                                              width: obj.width,
                                              color: obj.color,
                                              thumbImage: obj.urls.thumb,
                                              mediumImage: obj.urls.regular,
                                              largeImage: obj.urls.full,
                                              rawImage: obj.urls.raw,
                                              smallImage: obj.urls.small))
                print(obj.urls.raw)
            }
            self.refreshData?()
            self.pageNumber += 1
            
        }) { (serverErrorResponse, message) in
            self.isLoading = false
                
            if let message = serverErrorResponse?.message {
                self.errorMessage = message
                return
                       
            }
                                      
            //parse serverErrorResponse.errors for keybased errors
            if let message = message {
                self.errorMessage = message
                return
            }
            
        }
    }
    
    func getMoreImages (index:Int) {
        if index == (self.images.count - 1) {
            self.getImages()
        }
    }
    
    func setImageFavorite(index:Int) {
        
        let obj = self.images[index]
        let image = ImageModel(id: obj.id, urls: ImageURL(raw: obj.rawImage, full: obj.largeImage, regular: obj.mediumImage, small: obj.smallImage, thumb: obj.thumbImage), height: obj.height, width: obj.width, color: obj.color)
        
        imageRepository.saveFavoriteImage(image: image, completion: { (success) in
        
            
        }) { error in
            self.errorMessage = error ?? "Error"
        }
    }
}


struct HomeImages {
    var id: String
    
    var height: Int
    var width: Int
    var color: String
    
    let thumbImage:String
    let mediumImage:String
    let largeImage:String
    let rawImage:String
    let smallImage:String
}
