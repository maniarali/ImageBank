//
//  HomeViewController.swift
//  ImageGallery
//
//  Created by MA Maniar on 18/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import UIKit
import SDWebImage

class HomeView: BaseView {

    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = HomeViewModel()
        
        self.setCollectionView()
        self.setNavigationBar()
        
        self.bindWithReloadData()
        self.addGesture()
    }
    private func addGesture() {
        let gesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        self.collectionView.addGestureRecognizer(gesture)
    }
    @objc func pinchGesture (_ gesture: UIPinchGestureRecognizer) {
        let gestureScale =  Int(gesture.scale * 10.0) 
        if gestureScale > 1 && gestureScale < 9{
            DispatchQueue.main.async {
                self.setLayout(column: gestureScale)
            }
        }
    }
    
    private func setNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.navBar.fadeView(style: .bottom, percentage: 0.7)
        }
        
    }
    
    /// Private function to setCollectionView
    private func setCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
        self.setLayout(column: 2)

    }
    
    private func setLayout(column: Int) {
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        // Change individual layout attributes for the spacing between cells
        let distance = CGFloat(10 - column)
        layout.minimumColumnSpacing = distance
        layout.minimumInteritemSpacing = distance
        layout.columnCount = column
        
        layout.sectionInset = UIEdgeInsets(top: distance, left: distance, bottom: distance, right: distance)
        
        // Collection view attributes
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        collectionView.collectionViewLayout = layout
    }

    //MARK:- Bindings
    func bindWithReloadData(){
        (viewModel as! HomeViewModel).refreshData = {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }
    }
    
}

extension HomeView : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel as! HomeViewModel).images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.obj = (viewModel as! HomeViewModel).images[indexPath.row]
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (viewModel as! HomeViewModel).getMoreImages(index: indexPath.row)
    }
}
extension HomeView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
}
extension HomeView: CHTCollectionViewDelegateWaterfallLayout {
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
        let obj = (viewModel as! HomeViewModel).images[indexPath.row]
        let imageSize = CGSize(width: obj.width, height: obj.height)
        return imageSize
    }
}

extension HomeView : FavoriteDelegate {
    
    func didFavoriteImage(index: IndexPath) {
        (viewModel as! HomeViewModel).setImageFavorite(index: index.row)
    }
    
}
