//
//  HomeCell.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import UIKit
import SDWebImage

protocol FavoriteDelegate {
    func didFavoriteImage(index: IndexPath)
}

class HomeCell: UICollectionViewCell {

    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var heartPopup: UIImageView!
    
    var obj:HomeImages?{
        didSet{
            self.setHomeCell()
        }
    }
    var indexPath: IndexPath?
    var delegate:FavoriteDelegate?
    
    var liked = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let imageOpener = UITapGestureRecognizer(target: self, action: #selector(showImage(_:)))
        self.contentView.addGestureRecognizer(imageOpener)
        
        let likegesture = UILongPressGestureRecognizer(target: self, action: #selector(likeImage(_:)))
        self.contentView.addGestureRecognizer(likegesture)
        
        self.heartPopup.isHidden = true
    }
    
    @objc func likeImage ( _ gesutre : UILongPressGestureRecognizer) {
        if gesutre.state == .began {
           self.heartPopup.isHidden = false
            
           UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
               self.heartPopup.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
               self.heartPopup.alpha = 1.0
           }, completion: {(_ finished: Bool) -> Void in
               UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
                self.heartPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
               }, completion: {(_ finished: Bool) -> Void in
                   UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
                       self.heartPopup.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                       self.heartPopup.alpha = 0.0
                   }, completion: {(_ finished: Bool) -> Void in
                        self.heartPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        self.heartPopup.isHidden = false
                        
                        if self.indexPath != nil {
                            self.delegate?.didFavoriteImage(index: self.indexPath!)
                        }
                   })
               })
           })
        }
    }
    
    func setHomeCell() {
        guard let obj = obj else { return }
        self.homeImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        
        SDWebImageDownloader.shared.downloadImage(with: URL(string: obj.thumbImage)) { (image, data, error, isSuccess) in
            if image != nil {
                self.homeImage.sd_setImage(with: URL(string: obj.mediumImage)   )
            }
        }
        self.homeImage.clipsToBounds = true
        self.homeImage.backgroundColor = UIColor(hex: obj.color).withAlphaComponent(0.6)
        self.homeImage.layer.cornerRadius = 8.0
        self.homeImage.layer.borderWidth = 2.0
        self.homeImage.layer.borderColor = UIColor(hex: obj.color).withAlphaComponent(0.6).cgColor
        
    }
    
    // MARK:- IBActions
    // function to show image bigger
    @objc func showImage(_ gesutre : UITapGestureRecognizer) {
        if let post = obj {
            Helper.sharedInstance.showImage(inView: self.homeImage, urlString: post.largeImage)
        }
        
    }
}
