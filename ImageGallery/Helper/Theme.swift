//
//  Theme.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation
import UIKit

class AppTheme {
    
    private init() {}
    
    static func currentTheme() -> Theme {
        
        switch theme {
        case .dark: return Dark()
        case .light: return Light()
        }
    }
    static var theme: Themes = .dark
    enum Themes {
        case light
        case dark
    }
    
}

/// Theme class to handle colors in app.
protocol Theme {
    
    var primary: UIColor {get}
    var success: UIColor {get}
    var primaryTextColor: UIColor {get}
    

class Dark : Theme {
    var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .lightContent
        }
    }
    
    var secondaryBg: UIColor { return UIColor(hex: "2f2f2f") }
    
    var primaryImage: UIImage {return UIImage(named: "popup")!}
    var lightWhite: UIColor {return UIColor(hex: "f3f3f3")}
    
    var darkGrey: UIColor{return UIColor(hex : "3e4355")}
    var logo: UIImage { return UIImage(named: "logo")! }
    var keyboardAppearance: UIKeyboardAppearance = .dark
    
    var fontname: String { return "Poppins"}
    
    var primaryBackground:UIColor { return UIColor.black }
    var secondaryBackground:UIColor { return UIColor(hex: "2f2f2f") }
    
    var secondary:UIColor { return UIColor(hex: "00bbef") }
    var primary:UIColor { return UIColor(hex: "4ceaff") }
    
    var danger:UIColor { return UIColor.red }
    var success:UIColor { return UIColor(hex: "0f490f") }
    
    var primaryTextColor:UIColor { return UIColor.white }
    var secondaryTextColor:UIColor { return UIColor.white.withAlphaComponent(0.7) }
    
    var primaryButtonTextColor : UIColor { return UIColor.black }
    
    var sideMenuIconColor : UIColor {
          return UIColor(hex: "181818")
    }
//    var searchView : UIColor {
//        return UIColor(hex: "f2f2f2")
//    }
      var searchView : UIColor {
            return UIColor(hex: "2f2f2f")
        }
    
    
    var blackColor : UIColor {
        return UIColor.black.withAlphaComponent(1.0)
    }
    
}

class Light : Theme {
   var preferredStatusBarStyle: UIStatusBarStyle {
       if #available(iOS 13.0, *) {
           return .darkContent
       } else {
           return .lightContent
       }
   }
    
    var secondaryBg: UIColor { return UIColor.black}
    var primaryImage: UIImage {return UIImage(named: "popup_white")!}
    var darkGrey: UIColor {return UIColor(hex: "3e4355")}
    
    var lightWhite: UIColor {return UIColor(hex: "f3f3f3")}
    
    
    var logo: UIImage { return UIImage(named: "logo")! }
    var keyboardAppearance: UIKeyboardAppearance = .light
    
    var fontname: String { return "Poppins"}
    
    var primaryBackground:UIColor { return UIColor.white }
    var secondaryBackground:UIColor { return UIColor(hex: "f2f2f2") }
    
    var secondary:UIColor { return UIColor(hex: "4ceaff") }
    var primary:UIColor { return UIColor(hex: "00bbef") }
    
    var danger:UIColor { return UIColor.red }
    var success:UIColor { return UIColor(hex: "0f490f") }
    
    var primaryTextColor:UIColor { return UIColor.black }
    var secondaryTextColor:UIColor { return UIColor.black.withAlphaComponent(0.7) }
    
    var primaryButtonTextColor : UIColor { return UIColor.white }
    
    var sideMenuIconColor : UIColor {
          return UIColor(hex: "D5D5D5")
    }
    
    var searchView : UIColor {
       return UIColor(hex: "ffffff")
    }
    
    var blackColor : UIColor {
        return UIColor.black.withAlphaComponent(1.0)
    }
}
