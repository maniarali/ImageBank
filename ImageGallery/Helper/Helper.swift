//
//  Helper.swift
//  ImageGallery
//
//  Created by MA Maniar on 20/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation
import UIKit
import KSPhotoBrowser

class Helper {
    
    private init() {}
    
    static let sharedInstance: Helper = Helper()
    
    /// Function which opens Images
    /// - Parameter inView: view where image is to be shown
    /// - Parameter urlString: url of the image
    /// - Parameter vc: source view controller
    /// - Parameter isLocal: to judge image is local or to be downloaded. 
    /// - Parameter sourceImage: source image
    func showImage(inView : UIImageView, urlString : String = "", vc: UIViewController? = nil, isLocal: Bool = false, sourceImage: UIImage? = nil) {
        
        var items : [KSPhotoItem] = []
        
        let item : KSPhotoItem!
        if isLocal {
            item = KSPhotoItem(sourceView: inView, image: sourceImage)
        } else {
            item = KSPhotoItem(sourceView: inView, imageUrl: URL(string: urlString))
        }
        
        items.append(item)
        let browser = KSPhotoBrowser(photoItems: items, selectedIndex: 0)
        
        browser.dismissalStyle = .rotation
        browser.loadingStyle = .determinate
        browser.backgroundStyle = .blurPhoto
        
        if vc == nil {
            DispatchQueue.main.async {
                if let rootWindow = UIApplication.getTopViewController(){
                    rootWindow.present(browser, animated: true, completion: nil)
                }
            }
            
        } else {
            browser.show(from: vc!)
        }
        
    }
}
extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
