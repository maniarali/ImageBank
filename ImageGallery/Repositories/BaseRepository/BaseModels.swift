//
//  BaseModels.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

/// Class to Base Model
struct ServerErrorResponse: Decodable {
    var timestamp: String?
    var status: Int?
    var message: String?
    var errors: [[String: String]]
}

