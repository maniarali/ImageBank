//
//  BaseRepository.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation


/// Class to have base functionalities for Repository
class BaseRepository {
    
    var networkAdapter : NetworkChangeNotifiable = NetworkChangeClass()
    
    enum FetchingType {
        case remote
        case local
        case remoteUpdateLocal
        case localFetchingRemote
        case automatic
    }
    
}
