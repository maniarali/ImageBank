//
//  ImageModel.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

/// Strucutres of Images Model
struct ImageResultsModel : Decodable{
    var results: [ImageModel]
}
struct ImageModel : Decodable{
    var id: String
    var urls : ImageURL
    var height: Int
    var width: Int
    var color: String
    
}
struct ImageURL : Decodable {
    
    var raw: String
    var full: String
    var regular: String
    var small :String
    var thumb: String
}
