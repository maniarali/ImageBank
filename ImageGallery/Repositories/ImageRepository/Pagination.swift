//
//  Pagination.swift
//  ImageGallery
//
//  Created by MA Maniar on 20/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

/// Structure for pagination
struct Pagination: Decodable {
    var total_pages : Int
    var total : Int
    
}
