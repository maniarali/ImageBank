//
//  ImageRemoteSource.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation

class ImageRemoteRepository{
    
    /// Searching image from online data source
    /// - Parameter query: query string
    /// - Parameter pageNumber: reference of page
    /// - Parameter completion: callback after request completed
    func getSearchImages(query:String, pageNumber:Int, completion : @escaping (ImageResultsModel?,Pagination?, Failure?) -> ()){
        let endPoint = "search/photos"
        let appendingURL = "\(endPoint)?page=\(pageNumber)&query=\(query)&per_page=30&client_id=\(Enviroment.clientSecret)"
        Router.APIRouter(appendingURL: appendingURL, parameters: nil, method: .get) { (response) in
             
             switch response {
                 
             case .success(let success):
                 guard let notification = try? JSONDecoder().decode(ImageResultsModel.self, from: success.data) else {
                     completion(nil,nil,Failure(message: "Unable to parse notification", state: .unknown, data: nil, code: nil))
                     return
                 }
                 guard let page = try? JSONDecoder().decode(Pagination.self, from: success.data) else {
                     completion(notification,nil,Failure(message: "Unable to parse page", state: .unknown, data: nil, code: nil))
                     return
                 }
                 completion(notification,page,nil)
                 
             case .failure(let failure):
                 
                 completion(nil,nil,failure)
                 
             }
             
         }
         
     }
    
    /// Get All Images Sequentially
    /// - Parameter pageNumber: reference of page
    /// - Parameter completion: callback after request completed
    func getImages(pageNumber:Int, completion : @escaping ([ImageModel], Failure?) -> ()){
         
        let endPoint = "photos"
        let appendingURL = "\(endPoint)?page=\(pageNumber)&per_page=30&client_id=\(Enviroment.clientSecret)"
        Router.APIRouter(appendingURL: appendingURL, parameters: nil, method: .get) { (response) in
             
             switch response {
                 
             case .success(let success):
                 guard let notification = try? JSONDecoder().decode([ImageModel].self, from: success.data) else {
                     completion([],Failure(message: "Unable to parse notification", state: .unknown, data: nil, code: nil))
                     return
                 }
                
                 completion(notification,nil)
                 
             case .failure(let failure):
                 
                 completion([],failure)
                 
             }
             
         }
         
    }
     
}
