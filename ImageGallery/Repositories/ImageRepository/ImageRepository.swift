//
//  ImageRepository.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation
class ImageRepository : BaseRepository{
    
    //Refence of Remote Source
    public var remoteDataSource : ImageRemoteRepository!
    //Refence of Local Source
    public var localDataSource : ImageLocalRepository!
    
    override init() {
        super.init()
        
        self.remoteDataSource = ImageRemoteRepository()
        self.localDataSource = ImageLocalRepository()
    }
    
    
    /// Function in repository to save image as favorite
    /// - Parameter image: image object to set favorite
    /// - Parameter completion: callback after image is save
    /// - Parameter errorCompletion: callback if any error occurs
    func saveFavoriteImage(image:ImageModel, completion : @escaping (String?) -> () , errorCompletion : @escaping (String?) -> ()) {
        localDataSource.saveData(image) { (successMessage, error) in
            if let error = error {
                errorCompletion(error)
            } else {
                completion(successMessage)
            }
        }
    }
    /// Function in repository to remove image as favorite
    /// - Parameter id: id of image
    /// - Parameter completion: callback after image is remove
    /// - Parameter errorCompletion: callback if any error occurs in remove
    func unSaveFavoriteImage(id:String, completion : @escaping (String?) -> () , errorCompletion : @escaping (String?) -> ()) {
        localDataSource.deleteData(id: id) { (successMessage, error) in
            if let error = error {
                errorCompletion(error)
            } else {
                completion(successMessage)
            }
        }
    }
    
    /// Function to fetch Favorite image from database
    /// - Parameter completion: callback after image is save in favorite
    /// - Parameter errorCompletion: callback if any error occurs in calling favorite image
    func getFavoriteImage(completion : @escaping ([ImageModel]) -> () , errorCompletion : @escaping (String?) -> ()) {
        localDataSource.getData { (imageSource, error) in
            if let error = error {
                errorCompletion(error)
            } else {
                completion(imageSource)
            }
        }
    }
    
    /// Fetch images from server in listing manner
    /// - Parameter pageNumber: current page number
    /// - Parameter completion: callback after list of images are fetched
    /// - Parameter errorCompletion: callback if any error occurs
    func getImages(pageNumber:Int, completion : @escaping ([ImageModel]) -> () , errorCompletion : @escaping (ServerErrorResponse? , String?) -> ()) {
        
        
        remoteDataSource.getImages( pageNumber: pageNumber) { (imagesArray, error)  in
            guard error == nil else {
//                self.switchFailure(error!, errorCompletion: errorCompletion)
                return
            }
            
            completion(imagesArray)
            
            
        }
    }
    /// Fetch images from server after perform search in images
    /// - Parameter query: image to be search
    /// - Parameter pageNumber: current page number
    /// - Parameter completion: callback after list of images are fetched
    /// - Parameter errorCompletion: callback if any error occurs
    func getSearchImages(query:String, pageNumber:Int, completion : @escaping (ImageResultsModel?, Pagination?) -> () , errorCompletion : @escaping (ServerErrorResponse? , String?) -> ()) {
            
            
            remoteDataSource.getSearchImages(query: query, pageNumber: pageNumber) { (images, page, error)  in
                guard error == nil else {
                    return
                }
                
                if images != nil && page != nil {
                    completion(images!, page!)
                }
                
                
            }
        }
    
}
