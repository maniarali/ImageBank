//
//  ImageLocalSouce.swift
//  ImageGallery
//
//  Created by MA Maniar on 19/06/2020.
//  Copyright © 2020 MA. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ImageLocalRepository{
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    /// Funciton which save image in local databse
    /// - Parameter obj: image reference to be save
    /// - Parameter completion: callback after image is saved
    func saveData(_ obj: ImageModel, completion : @escaping (String?, String?) -> ()) {
        
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Images", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(obj.urls.thumb, forKey: "thumbImage")
        newUser.setValue(obj.urls.regular, forKey: "mediumImage")
        newUser.setValue(obj.urls.full, forKey: "largeImage")
        newUser.setValue(obj.urls.raw, forKey: "rawImage")
        newUser.setValue(obj.urls.raw, forKey: "smallImage")
        
        newUser.setValue(obj.color, forKey: "color")
        newUser.setValue(obj.height, forKey: "height")
        newUser.setValue(obj.width, forKey: "width")
        newUser.setValue(obj.id, forKey: "id")
        
        do {
            try context.save()
            completion("Image save successfully", nil)
        } catch {
            completion(nil, "Error saving in Local DB")
        }
    }
    
    /// Function to delete specific image from database
    /// - Parameter id: id of image
    /// - Parameter completion: callback after image is deleted
    func deleteData(id:String, completion : @escaping (String?, String?) -> ()) {
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Images")
        fetchRequest.predicate = NSPredicate.init(format:"id = %@", id as CVarArg)
        let result = try? context.fetch(fetchRequest)
        let resultData = result as! [Images]
        
        for object in resultData {
            context.delete(object)
        }
        
        do {
            try context.save()
            completion("Image un save successfully", nil)
        } catch let error as NSError  {
            completion(nil, "Error un saving in Local DB")
        } catch {
            
        }
    }
    
    /// Get List of all saved image
    /// - Parameter completion: callback with all fetch image
    func getData(completion : @escaping ([ImageModel], String?) -> ()) {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Images")
        
        request.returnsObjectsAsFaults = false
        var imageLocalData = [ImageModel]()
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                let thumbImage = data.value(forKey: "thumbImage") as! String
                let mediumImage = data.value(forKey: "mediumImage") as! String
                let largeImage = data.value(forKey: "largeImage") as! String
                let rawImage = data.value(forKey: "rawImage") as! String
                let smallImage = data.value(forKey: "smallImage") as! String
                
                let color = data.value(forKey: "color") as! String
                let height = data.value(forKey: "height") as! Double
                let width = data.value(forKey: "width") as! Double
                let id = data.value(forKey: "id") as! String
                
                let image = ImageModel(id: id, urls: ImageURL(raw: rawImage, full: largeImage, regular: mediumImage, small: smallImage, thumb: thumbImage), height: Int(height), width: Int(width), color: color)
                imageLocalData.append(image)
            }
            completion(imageLocalData, nil)
            
        } catch {
            
            completion([], "Error fetching")
        }
    }
    
    
}
